#!/bin/zsh

input="students_fab.txt"
while IFS= read -r VAR
do
  link=$(echo $VAR|awk -F, '{print $1}')
  name=$(echo $VAR|awk -F, '{print $2}')
  name_normal=$(echo "$name" | awk '{print tolower($0)}')
  name_under=$(echo ${name_normal// /_})
  # echo $VAR | awk -F, '{print $1}'
  echo '<div class="col-sm-3">' >> links.txt
  echo '<a href="'$link'">' >> links.txt
  echo '<img src="home/img/'$name_under'.png" class="img-circle" width="180" height="180">' >> links.txt
  echo '</a>' >> links.txt
  echo '<h5>'$name'</h5>' >> links.txt
  echo '<br>' >> links.txt
  echo '</div>' >> links.txt
done <"$input"
